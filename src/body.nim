# SPDX-FileCopyrightText: 2021 Hector A. Escobedo <hector.escobedo@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-only

# creature body structures

import tables, hashes
import sequtils, sugar

import utils

type
  Capability* = enum
    bite, grab, hear, see, smell, taste
  Direction3D* = enum
    up, down, left, right, front, back

  BodyPart* = ref object
    description*: string
    capabilities*: set[Capability]
  # tracking the connections between parts
  # (A,Dir) key, B value means "going Dir from A to B"
  # example: (head, down) = thorax
  Body* = TableRef[(BodyPart, Direction3D), BodyPart]

func hash(x: BodyPart): Hash =
  var h: Hash = x.description.hash !& x.capabilities.hash
  result = !$ h

func describe*(part: BodyPart): string =
  if part.capabilities.card > 0:
    let capDesc = map(part.capabilities.toSeq, x => $x).describe
    return part.description & " that can " & capDesc
  else:
    return part.description

func describe*(body: Body): string =
  for k, partB in body.pairs:
    result.add(partB.describe & "\n")
    # need to deduplicate parts and figure out how to extract from key tuple

func mirror*(d: Direction3D): auto =
  case d
  of up: down
  of down: up
  of left: right
  of right: left
  of front: back
  of back: front

assert(mirror(up) == down)
assert(mirror(right) == left)
assert(mirror(front) == back)

# procedure to add appropriate mirror entry as well
proc attachParts*(body: Body, partA: BodyPart, dir: Direction3D, partB: BodyPart) =
  body[(partA, dir)] = partB
  body[(partB, mirror(dir))] = partA

proc newHumanoidSpeciesBody*(sp: string = "human"): Body =
  let
    thorax = BodyPart(description: sp & " thorax", capabilities: {})
    abdomen = BodyPart(description: sp & " abdomen")
    pelvis = BodyPart(description: sp & " pelvis")
    head = BodyPart(description: sp & " head",
                    capabilities: {bite, hear, see, smell, taste})
    leftArm = BodyPart(description: sp & " arm", capabilities: {grab})
    rightArm = BodyPart(description: sp & " arm", capabilities: {grab})
    leftLeg = BodyPart(description: sp & " leg")
    rightLeg = BodyPart(description: sp & " leg")
  var
    body: Body

  new(body)
  attachParts(body, head, down, thorax)
  attachParts(body, thorax, right, rightArm)
  attachParts(body, thorax, left, leftArm)
  attachParts(body, thorax, down, abdomen)
  attachParts(body, abdomen, down, pelvis)
  attachParts(body, pelvis, right, rightLeg)
  attachParts(body, pelvis, left, leftLeg)
  return body
