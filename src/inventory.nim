# SPDX-FileCopyrightText: 2021 Hector A. Escobedo <hector.escobedo@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-only

# inventory and items system

type
  ItemCategory* = enum
    container, armor, weapon, potion
  ArmorCategory* = enum
    light, medium, heavy
  WeaponCategory* = enum
    meleeSimple, meleeMartial, rangedSimple, rangedMartial, ammunition
  PotionCategory* = enum
    healing, poison

  Item* = ref object
    description*: string
    weight*: Natural
    case category*: ItemCategory
    of container:
      capacity*: Natural
      contents*: Inventory
    of armor:
      armorCategory*: ArmorCategory
      armorClassBase*: Natural
    of weapon:
      damage*: Natural # TODO replace with random generator
      case weaponCategory*: WeaponCategory
      of meleeSimple, meleeMartial:
        reach*: Natural
      of rangedSimple, rangedMartial:
        normalRange*: Natural
        longRange*: Natural
      of ammunition: discard
    of potion:
      case potionCategory*: PotionCategory
      of healing, poison:
        hpChange*: int
  Inventory* = seq[Item]

proc newEmptyBackpack*(): auto =
  Item(description: "backpack",
       weight: 5,
       category: container,
       capacity: 50)

proc newLeatherArmor*(): auto =
  Item(description: "leather armor",
       weight: 10,
       category: armor,
       armorCategory: light,
       armorClassBase: 11)

proc newShortsword*(): auto =
  Item(description: "shortsword",
       weight: 2,
       category: weapon,
       weaponCategory: meleeMartial,
       damage: 4)

proc newHealingPotion*(): auto =
  Item(description: "healing potion",
       weight: 1,
       category: potion,
       potionCategory: healing,
       hpChange: 10)
