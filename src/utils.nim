# SPDX-FileCopyrightText: 2021 Hector A. Escobedo <hector.escobedo@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-only

# some common utility functions

import sequtils

func describe*(things: seq[string]): string =
  case things.len
  of 0: ""
  of 1: things[0]
  of 2: things[0] & " and " & things[1]
  else: foldl(things[0 .. ^2], a & ", " & b) & ", and " & things[^1]
