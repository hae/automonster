# SPDX-FileCopyrightText: 2021 Hector A. Escobedo <hector.escobedo@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-only

# Main module for automonster executable

import random

import body
import inventory
import utils

type
  Creature* = object
    species: string
    body: Body
    hp: int
    atk: int
    def: int
    inventory: Inventory

func describe*(cr: Creature): string =
  result = "A " & cr.species & "."
  result.add(" It has " & $cr.hp & " hit points.")
  result.add(" It has an attack of " & $cr.atk & " and a defense of " & $cr.def & ".")
  if cr.inventory.len > 0:
    var inventoryStrings: seq[string]
    for item in cr.inventory:
      inventoryStrings.add(item.description)
    result.add(" It's carrying: " & inventoryStrings.describe & ".")

proc main() =
  randomize()
  var sampleHuman = Creature(species: "human",
                             body: newHumanoidSpeciesBody(),
                             hp: rand(10 .. 100),
                             atk: rand(2 .. 20),
                             def: rand(2 .. 20))
  sampleHuman.inventory.add(newLeatherArmor())
  sampleHuman.inventory.add(@[newShortsword(), newHealingPotion()])
  echo sampleHuman.describe
  echo sampleHuman.body.describe
  let sampleBear = Creature(species: "Bear", hp: rand(100 .. 200),
                            atk: rand(10 .. 30), def: rand(10 .. 30))
  echo sampleBear.describe

when isMainModule:
  main()
