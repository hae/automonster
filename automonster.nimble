# SPDX-FileCopyrightText: 2021 Hector A. Escobedo <hector.escobedo@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-only

# Package

version       = "0.1.0"
author        = "Hector A. Escobedo"
description   = "Generate monster stats"
license       = "GPL-3.0"
srcDir        = "src"
bin           = @["automonster"]


# Dependencies

requires "nim >= 1.4.0"
